# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2021-12-05

### Changed

- Errors type was change from String to eyre's Error.
- Fields and structures became public.

### Added

- Find method to look through answers.
- `as_str` method to receive text representation of an answer.

## [0.1.0] - 2021-06-04

### Added

- Initial functionality to receive a list of form's responses.

[Unreleased]: https://gitlab.com/humb1t/typeform-rs/compare/v0.2.0...HEAD
[0.2.0]: https://gitlab.com/humb1t/typeform-rs/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/humb1t/typeform-rs/releases/tag/v0.1.0
